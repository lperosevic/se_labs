
import pygame
import random
 

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
GREEN   = (0,   128,   0)
 
class Block(pygame.sprite.Sprite):
    """
    Klasa potjece od Sprite klase u Pygame.
    Sprite klasa je roditeljska klasa.
    """
 
    def __init__(self, color, width, height):
     
 
        # Pozivanje roditeljske klase
        super().__init__()
 
      
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
 
        self.rect = self.image.get_rect()
 

pygame.init()

screen_width = 700
screen_height = 400
screen = pygame.display.set_mode([screen_width, screen_height])
 

# Lista blokova ali ne ukljucuje blok kojeg igrac pomice
block_list = pygame.sprite.Group()
 
# Lista svih blokova ili kvadratica. Ukljucuje i igraca
all_sprites_list = pygame.sprite.Group()
 
for i in range(50):
   
    block = Block(BLACK, 20, 15)
 
    """ 
    Stavljeno -20 i -15 u redu tako da blok bude unutar slike.
    Inace se moze dogoditi da se ne vidi cijeli
    """

    block.rect.x = random.randrange(screen_width -20)
    block.rect.y = random.randrange(screen_height -15)
 
    block_list.add(block)
    all_sprites_list.add(block)
 

player = Block(GREEN, 20, 15)
all_sprites_list.add(player)
 
# Loop dok korisnik ne stisne close button
done = False
 
# Clock se koristi da znamo koliko se slika brzo refresha
clock = pygame.time.Clock()
 
score = 0
 
# -------- Main Program Loop -----------
while not done:
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            done = True
 
    # Cisti sliku
    screen.fill(WHITE)
 
    # Dohvaca trenutnu poziciju misa i vraca poziciju kao listu 2 broja x i y
    pos = pygame.mouse.get_pos()
 
    
    # Postavlja igracev objekt na lokaciju misa
    player.rect.x = pos[0]
    player.rect.y = pos[1]
 
    # Provjera da li se igracev blok sudario s nekim objektom, ako je brise onog koji se sudario s igracem
    # Ako je postavljeno na False onda se ne bi brisao 
    blocks_hit_list = pygame.sprite.spritecollide(player, block_list, True)
 
    # Povecava score za 1 kad se sudari s blokom
    for block in blocks_hit_list:
        score += 1
        print("Score:", score)
 
    
    all_sprites_list.draw(screen)
 
    # Azurira sliku
    pygame.display.flip()
 
    # Osvjezava sliku s 60 framea u sekundi
    clock.tick(60)
 
pygame.quit()
