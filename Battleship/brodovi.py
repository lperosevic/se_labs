from random import randint

board = []  #polje nazvano board

for x in range(0, 5):
  board.append(["O"] * 5)  #radi 5x5 polje znakova 'O'

def print_board(board):
  for row in board:
    print " ".join(row)  #join znaci da dodaje row podatak, def print_board je funkcija koja crta polje 5x5

print_board(board)  #ovdje se poziva funkcija

def random_row(board):
  return randint(0, len(board) - 1)  #funkcija za postavljanje broda na random red, len(board)-1 zato sto ide preko indeksa od 0 do 4...
				     #...len(board)-1 je dobro koristit jel ako promijenimo velicinu polja uvijek ce ici od prvog do zadnjeg
def random_col(board):
  return randint(0, len(board[0]) - 1)  #funkcija za postavljanje broda na random stupac

ship_row = random_row(board)  #pozivi funkcija, ako zelis varati i znati di ih je random stavio onda bi... 
ship_col = random_col(board)  #...upisao ispod ovih poziva funkcija print ship_row i print ship_col


# Everything from here on should be in your for loop
# don't forget to properly indent!
for turn in range(4):                     #stavljeno je da imaa 4 pokusaja da se pogodi
  print "Turn", turn + 1		  #ispisuje na ekran na kojem smo pokusaju i povecava za 1
  guess_row = int(raw_input("Guess Row: "))  #unos int vrijednosti reda (unosi se od 0 do 4)
  guess_col = int(raw_input("Guess Col: "))  #unos int vrijednosti stupca (unosi se od 0 do 4)

  if guess_row == ship_row and guess_col == ship_col:  #ako se pogodi redak i stupac di je brod stavio onda je brod potopljen i pobjeda
    print "Congratulations! You sank my battleship!"   
    break
  else:
    if guess_row not in range(5) or guess_col not in range(5):
      print "Oops, that's not even in the ocean."
    elif board[guess_row][guess_col] == "X":    #u slucaju da stavis red i stupac koji si vec prije rekao u nekom pokusaju
      print( "You guessed that one already." )
    else:
      print "You missed my battleship!"       #javlja svaki put kad promasis brod
      board[guess_row][guess_col] = "X"	      #stavlja X na lokaciju koju si promasio
    if (turn == 3):
      print "Game Over"                      #ako ne pogodis u 4 pokusaja gubis
      print ship_row			     #ispisuje u kojem se redu brod nalazio
      print ship_col			     #ispisuje u kojem se stupcu brod nalazio
    print_board(board)			     #crta cijelo polje, vrlo je bitno uvlacenje koda u pythonu 
